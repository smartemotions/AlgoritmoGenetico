import sys
import copy
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from Punto import Punto
from Grafo import Grafo
from Arista import Arista
from Lienzo import *
from Places import *
form_class = uic.loadUiType("AG-GUI.ui")[0]
class Ventana(QMainWindow, form_class):
	'''
	Clase que implementa las ventana principal del programa AG V 1.0
	esta clase contiene todos los objetos que se heredan de una ventana creada en
	QtDesigner, dentro del proyecto del programa se encuentra el archivo de ventana
	ademas se adicionan otros objetos para las funciones necesarias del programa
	'''
	def __init__(self, parent=None):
		QtGui.QMainWindow.__init__(self, parent)
		self.numerodenodos = 0
		self.setupUi(self)
		self.lienzoGraphic = LienzoDibujo(self)
		self.lienzoLayout.addWidget(self.lienzoGraphic)
		
		self.saveasTXT = QtGui.QAction('Guardar Grafo', self)
		self.openTXT = QtGui.QAction('Abrir Grafo', self)
		
		menubar = self.menuBar()
		fileMenu = menubar.addMenu('&Archivo')
		fileMenu.addAction(self.openTXT)
		fileMenu.addAction(self.saveasTXT)
		
		self.connect(self.openTXT, QtCore.SIGNAL('triggered()'), self.opentxtDialog)
		
		self.puntos = []
		self.aristas = []
		self.grafo = Grafo()
		self.contador = 0
		self.cantidad = 0
		self.x = ''
		self.y = ''
		self.opentxtDialog()
	
	def getGrafo(self):
		return self.grafo

	def opentxtDialog(self):
		'''
		Funcion que permite abrir un archivo de texto
		con formato del programa Rutas Maritimas, el metodo
		permite cargar un archivo de tipos *txt o *bin  cuando
		se carga un archivo, el grafo que se haya trabajado
		hasta el momento se eliminara, y solamente aparecera el nuevo grafo
		con el que se podra seguir trabajando normalmente
		 '''
		filename = 'AG.txt'
		if filename:
			self.limpiarLienzo()
			regTree = open(filename, "r")
			self.numerodenodos = regTree.readline()
			self.numerodenodos = int(self.numerodenodos.replace('\n', ''))
			i = 1
			for line in regTree:
				line = line.replace("\n", "")
				if line != '':
					line = line.split("|")
					if i <= self.numerodenodos + 1:
						if len(line) == 4:
							x = int(line[0])
							y = int(line[1])
							nombre = line[2]
							imagen = line[3]
							punto = Punto(x, y, nombre, imagen)
							self.grafo.ingresarNodo(nombre)
							self.puntos.append(punto)
							
					if self.numerodenodos < i:
						x1 = int(line[0])
						y1 = int(line[1])
						x2 = int(line[2])
						y2 = int(line[3])
						peso = float(line[4])
						nombre1 = line[5]
						nombre2 = line[6]
						punto1 = Punto(x1, y1, nombre1,'')
						punto2 = Punto(x2, y2, nombre2, '')
						arista = Arista(punto1, punto2, peso)
						self.aristas.append(arista)
						self.grafo.adicionarEnlace(nombre1, nombre2, peso)
				i = i + 1
			regTree.close()
			self.labelMessage.setText('Grafo abierto correctamente')
			
		self.lienzoGraphic.repaint()
			
	def limpiarLienzo(self):
		'''
		Metodo implementado para limpiar el 
		grafo que se encuetre en el lienzo
		esto permitira crer nuevos grafos desde el principio
		'''
		self.puntos = []
		self.aristas = []
		self.grafo.nombres = []
		self.grafo.nodos = []
		self.grafo.aristas = []
		self.lienzoGraphic.neo = []
		self.lienzoGraphic.repaint()
	  

if __name__ == "__main__":
	'''
	Inicio principal de la ventana y las clases para la ejecucion del programa
	'''
	app = QtGui.QApplication(sys.argv)
	MyWindow = Ventana(None)
	MyWindow.setWindowTitle(('Busqueda Grafo - AG').center(150, " "))
	screen = QtGui.QDesktopWidget().screenGeometry()
	size = MyWindow.geometry()
	MyWindow.move((screen.width() - size.width())/2, (screen.height() - size.height())/2)
	MyWindow.show()
	app.exec_()
