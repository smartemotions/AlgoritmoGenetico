import random
class Individuo():
	def __init__(self):
		self.camino = []
		self.genes = []
		self.destino = ''
		self.correct = False
		
	def generarIndividuo(self, punto, grafo):
		relaciones = []                        #Vector que guarda las relaciones
		for link in grafo.aristas:             #Recorrer las conexiones del mapa
			if link.getInicial() == punto:     #Punto partida c.otro nodo
				relaciones.append([link.getInicial(),
				                   link.getTerminal(),
				                   link.getPeso()])
			if link.getTerminal() == punto:
				relaciones.append([link.getTerminal(),
				                   link.getInicial(),
				                   link.getPeso()])
		freeLinks = []              #Acumulador para contar opciones salida
		for relacion in relaciones: #Recorre relaciones punto referencia
			if relacion[1] not in self.camino :
				freeLinks.append(relacion) #Se suma si el camino no esta cerrado
		if len(freeLinks) > 0:      #No cerrado se continua reproduciendo	
			if len(freeLinks)== 1:
				step = freeLinks[0]
			else:
				step = freeLinks[random.randrange(0,(len(freeLinks)))]
				
			if self.reproduccionTerminada(step[1]):
				self.camino.append(self.destino)
				self.genes.append(step[2])
				self.correct = True
				return True
			else:
				self.camino.append(step[1])
				self.genes.append(step[2])
				return self.generarIndividuo(step[1], grafo)
		return False
		
	def puntoDestino(self, destino):
		self.destino = destino
		
	def puntoInicio(self, inicio):
		self.camino.append(inicio)
		
	def reproduccionTerminada(self, destino):
		if self.destino == destino:
			return True
		else:
			return False
		
	def obtenerGenes(self):
		return self.genes
				
	def obtenerCamino(self):
		return self.camino
		
	def obtenerInicio(self):
		return self.camino[0]
		
	def obtenerDestino(self):
		return self.camino[-1]
		
	def setGenes(self, genes):
		self.genes = genes
		
	def setCamino(self, camino):
		self.camino = camino
		
	def obtenerFitness(self):
		fitness = 0
		for gen in self.genes:
			fitness += gen
		return fitness
		
	def restart(self):
		self.camino = []
		self.genes = []
		self.destino = ''
